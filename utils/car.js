const fs = require('fs');

// function to get all data in cars.json
const loadCars = () => {
    const fileBuffer = fs.readFileSync('data/cars.json', 'utf-8');
    const cars = JSON.parse(fileBuffer);
    return cars;
}

// export function loadCars
module.exports = { loadCars };