<p align="center">
<img src="./public/images/index.png" width="500">
<img src="./public/images/add.png" width="500">
</p>

# Challenge FSW Chapter 5 - UI
## Tentang desain web ini

Desain web ini merupakan kelanjutan dari project [CarManagementDashboard-HTML](https://gitlab.com/Yuzyzy88/carmanagementdashboard-html.git) menggunakan View Engine in Node.js untuk memenuhi kebutuhan manajemen data mobil. Dibuat dengan menggunakan:

- [Bootstrap v5.0](https://getbootstrap.com/).
- [Express.js 4.17.3](http://expressjs.com/).
- [Template engine ejs](https://github.com/mde/ejs).
- [npm v8.5.0.](https://docs.npmjs.com/cli/v6/commands/npm-install)
- [Node.js v16.14.2.](https://nodejs.org/en/)
  

Yang harus diinstall :
- npm install 

Dijalankan dengan :
- nodemon index.js

## Info

Desain menggunakan template dasar dari [SafnaPrasetiono](https://github.com/SafnaPrasetiono/Dashboard-Part1) dan dirubah mengikuti design [Binar Academy](https://www.figma.com/file/H6xTtBW9Kzlf09nYnitvbH/BCR---Car-Management-Dashboard?node-id=18343%3A5831)