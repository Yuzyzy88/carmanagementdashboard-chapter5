// import modul
const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const { loadCars } = require('./utils/car');

// use http://localhost:8000/
const app = express();
const port = 8000;

app.use(express.json()) // for parsing application/ json
app.use(express.urlencoded({ extended: true })) // for parsing application/ x-www-form-urlencoded
app.use(expressLayouts);
app.use(express.static('public')); // serve static files
app.set('view engine', 'ejs');

// index page
app.get('/', (req, res) => {
    const cars = loadCars(); // to get list cars
    res.status(200).render('index', {
        layout: 'layouts/main-layout',
        title: 'List Car',
        cars,
    });
});

// add page
app.get('/add', (req, res) => {
    res.status(200).render('add', {
        layout: 'layouts/main-layout',
        title: 'Add Car'
    });
});

// update page
app.get('/update/:name', (req, res) => {
    res.status(200).render('update', {
        layout: 'layouts/main-layout',
        title: 'Update Car',
    });
});


// if the request page is not recognized
app.use((req, res) => {
    res.status(400).send('404 not found');
})

// to run port 8000
app.listen(port, () => {
    console.log(`App listening http://localhost:${port}`);
});
